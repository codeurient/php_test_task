<?php


header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="sample.csv"');
$data = array(
    'ID,Name,Surname,Sex,BirthDate',
    '123,John,Doe,M,01.01.1990',
    '456,Jane,Smith,F,02.02.1985',
    '789,Bob,Johnson,M,03.03.1980'
);

$fp = fopen('php://output', 'wb');
foreach ( $data as $line ) {
    $val = explode(",", $line);
    fputcsv($fp, $val);
}
fclose($fp);