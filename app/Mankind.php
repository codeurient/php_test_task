<?php

namespace App;


use ArrayAccess;
use DateTime;
use Iterator;

class Mankind implements ArrayAccess, Iterator {
    private static ?Mankind $instance = null;
    private array $persons = [];
    private int $position = 0;

    private function __construct() {}

    public static function getInstance(): Mankind {
        if (self::$instance == null) {
            self::$instance = new Mankind();
        }
        return self::$instance;
    }

    public function addPerson(Person $person): void {
        $this->persons[$person->getId()] = $person;
    }

    public function getPerson(int $id): Person {
        return $this->persons[$id];
    }

    public function offsetExists($offset): bool {
        return isset($this->persons[$offset]);
    }

    public function offsetGet($offset): Person {
        return $this->getPerson($offset);
    }

    public function offsetSet($offset, $value): void {
        $this->addPerson($value);
    }

    public function offsetUnset($offset): void {
        unset($this->persons[$offset]);
    }

    public function rewind(): void {
        $this->position = 0;
    }

    public function current(): Person {
        return $this->persons[array_keys($this->persons)[$this->position]];
    }

    public function key(): int {
        return array_keys($this->persons)[$this->position];
    }

    public function next(): void {
        ++$this->position;
    }

    public function valid(): bool {
        return isset(array_keys($this->persons)[$this->position]);
    }

    public function getPersonById(int $id): ?Person
    {
        return $this->people[$id] ?? null;
    }

    public function getPercentageOfMen(): float {
        $numMen = 0;
        foreach ($this->persons as $person) {
            if ($person->getSex() == 'M') {
                $numMen++;
            }
        }
        return ($numMen / count($this->persons)) * 100;
    }


    public function loadFromCSVString(string $csvString, int $chunkSize = 100): void {
        $handle = fopen('php://memory', 'r+');
        fwrite($handle, $csvString);
        rewind($handle);

        while (!feof($handle)) {
            $chunk = [];

            for ($i = 0; $i < $chunkSize; $i++) {
                $data = fgetcsv($handle, 1000, ';');
                if ($data === false) {
                    break;
                }
                $id = intval($data[0]);
                $name = $data[1];
                $surname = $data[2];
                $sex = $data[3];
                $birthDate = DateTime::createFromFormat('d.m.Y', $data[4]);
                $person = new Person($id, $name, $surname, $sex, $birthDate);
                $chunk[] = $person;
            }
            foreach ($chunk as $person) {
                $this->addPerson($person);
            }
        }
        fclose($handle);
    }



}

