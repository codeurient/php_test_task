<?php

namespace App;

use DateTime;

class Person {
    private int $id;
    private string $name;
    private string $surname;
    private string $sex;
    private DateTime $birthdate;

    public function __construct(int $id, string $name, string $surname, string $sex, DateTime $birthdate) {
        $this->id = $id;
        $this->name = $name;
        $this->surname = $surname;
        $this->sex = $sex;
        $this->birthdate = $birthdate;
    }

    public function getId(): int {
        return $this->id;
    }

    public function getName(): string {
        return $this->name;
    }

    public function getSurname(): string {
        return $this->surname;
    }

    public function getSex(): string {
        return $this->sex;
    }

    public function getBirthdate(): DateTime {
        return $this->birthdate;
    }

    public function getAgeInDays(): int {
        $now = new DateTime();
        $interval = $this->birthdate->diff($now);
        return $interval->days;
    }
}